﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProServicesWebsite.Models
{
    public class Profesion
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(50, ErrorMessage = "La longitud máxima es de 50 caracteres")]
        [MinLength(4, ErrorMessage = "La longitud mínima es de 4 caracteres")]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProServicesWebsite.Models
{
    public class Contrato
    {
        public int ID { get; set; }

        public int UsuarioID { get; set; }
        public Usuario Usuario { get; set; }
        public int ProfesionalID { get; set; }
        public Profesional Profesional { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProServicesWebsite.Models
{
    public class Profesional
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(100, ErrorMessage = "Maximo 100 caracteres")]
        [MinLength(2, ErrorMessage = "La longitud mínima es de 2 caracteres")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(100, ErrorMessage = "Maximo 100 caracteres")]
        [MinLength(10, ErrorMessage = "La longitud mínima es de 10 caracteres")]
        [EmailAddress(ErrorMessage = "Por favor ingrese un mail válido")]
        [Display(Name = "E-Mail")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(30, ErrorMessage = "Maximo 30 caracteres")]
        [MinLength(8, ErrorMessage = "La longitud mínima es de 8 caracteres")]
        [RegularExpression("^[0-9]*$",ErrorMessage = "Por favor ingrese un teléfono válido")]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Tarifa por hora")]
        public int ValorHora { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Calificación")]
        public int Calificacion { get; set; }

        public int ImagenID { get; set; }

        [Display(Name = "Imágen")]
        public Imagen Imagen { get; set; }

        public int ProfesionID { get; set; }

        [Display(Name = "Profesiones")]
        public Profesion Profesion { get; set; }
        
        public virtual ICollection<Contrato> Usuarios { get; set; }
    }
}

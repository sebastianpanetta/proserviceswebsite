﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProServicesWebsite.Models
{
    public class Usuario
    {
        
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(100, ErrorMessage = "La longitud máxima es de 100 caracteres")]
        [MinLength(10, ErrorMessage = "La longitud mínima es de 10 caracteres")]
        [Display(Name = "Correo electrónico")]
        [EmailAddress(ErrorMessage = "El campo debe ser una dirección de correo electrónico")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(100, ErrorMessage = "La longitud máxima es de 100 caracteres")]
        [MinLength(2, ErrorMessage = "La longitud mínima es de 2 caracteres")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [MaxLength(100, ErrorMessage = "La longitud máxima es de 100 caracteres")]
        [MinLength(2, ErrorMessage = "La longitud mínima es de 2 caracteres")]
        [Display(Name = "Apellido")]
        public string Apellido { get; set; }

        [Display(Name = "Contraseña")]
        [MinLength(4, ErrorMessage = "La longitud mínima es de 4 caracteres")]
        public byte[] Contrasenia { get; set; }

        [Display(AutoGenerateField = false)]
        public DateTime? FechaUltimoAcceso { get; set; }

        public virtual ICollection<Contrato> Profesionales { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProServicesWebsite.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace ProServicesWebsite.Models
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ProfesionalesDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ProfesionalesDbContext>>()))
            {
                // Look for any board games already in database.
                if (context.Profesionales.Any())
                {
                    return;   // Database has been seeded
                }

                if (context.Profesiones.Any())
                {
                    return;
                }

                if (context.Imagenes.Any())
                {
                    return;
                }

                if (context.Usuarios.Any())
                {
                    return;
                }

                context.Profesionales.AddRange(
                    new Profesional
                    {
                        Nombre = "Profesional 1",
                        Mail = "profesional1@gmail.com",
                        Telefono = "4555-7899",
                        ValorHora = 300,
                        Calificacion = 4,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-1.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Electricista / Plomero"
                        }
                    },
                    new Profesional
                    {
                        Nombre = "Profesional 2",
                        Mail = "profesional2@gmail.com",
                        Telefono = "4966-5800",
                        ValorHora = 400,
                        Calificacion = 5,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-2.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Plomero"
                        }
                    },
                    new Profesional
                    {
                        Nombre = "Profesional 3",
                        Mail = "profesional3@gmail.com",
                        Telefono = "3333-0900",
                        ValorHora = 230,
                        Calificacion = 3,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-3.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Gasista"
                        }
                    },
                    new Profesional
                    {
                        Nombre = "Profesional 4",
                        Mail = "profesional4@gmail.com",
                        Telefono = "5555-5800",
                        ValorHora = 700,
                        Calificacion = 5,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-4.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Albañil / Plomero"
                        }
                    },
                    new Profesional
                    {
                        Nombre = "Profesional 5",
                        Mail = "profesional5@gmail.com",
                        Telefono = "1234-5678",
                        ValorHora = 560,
                        Calificacion = 4,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-5.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Electricista"
                        }
                    },
                    new Profesional
                    {
                        Nombre = "Profesional 6",
                        Mail = "profesional6@gmail.com",
                        Telefono = "4966-7766",
                        ValorHora = 450,
                        Calificacion = 5,
                        Imagen = new Imagen
                        {
                            Nombre = "profesional-6.jpg"
                        },
                        Profesion = new Profesion
                        {
                            Descripcion = "Plomero / Electricista / Albañil"
                        }
                    }
                    );

                context.Imagenes.AddRange(
                    new Imagen
                    {
                        Nombre = "profesional-7.jpg"
                    },
                    new Imagen
                    {
                        Nombre = "profesional-8.jpg"
                    },
                    new Imagen
                    {
                        Nombre = "profesional-9.jpg"
                    }
                    );

                context.SaveChanges();
            }
        }
    }
}

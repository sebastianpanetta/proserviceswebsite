﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ProServicesWebsite.Models
{
    public class Imagen
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Nombre de la imagen")]
        public string Nombre { get; set; }
    }
}

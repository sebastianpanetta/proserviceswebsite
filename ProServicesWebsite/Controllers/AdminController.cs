﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProServicesWebsite.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            if(!User.Identity.IsAuthenticated)
            {
                return View("Admin");
            } else 
            {
                return NotFound();
            }
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProServicesWebsite.Database;
using ProServicesWebsite.Models;

namespace ProServicesWebsite.Controllers
{
    public class ProfesionalController : Controller
    {
        private readonly ProfesionalesDbContext _context;

        public ProfesionalController(ProfesionalesDbContext context)
        {
            _context = context;
        }

        // GET: Profesional
        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return NotFound();
            }
            var profesionalesDbContext = _context.Profesionales.Include(p => p.Imagen).Include(p => p.Profesion);
            return View(await profesionalesDbContext.ToListAsync());
        }

        // GET: Profesional/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales
                .Include(p => p.Imagen)
                .Include(p => p.Profesion)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (profesional == null)
            {
                return NotFound();
            }

            return View(profesional);
        }

        // GET: Profesional/Create
        public IActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                return NotFound();
            }
            ViewData["ImagenID"] = new SelectList(_context.Imagenes, "ID", "Nombre");
            ViewData["ProfesionID"] = new SelectList(_context.Profesiones, "ID", "Descripcion");
            return View();
        }

        // POST: Profesional/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Nombre,Mail,Telefono,ValorHora,Calificacion,ImagenID,ProfesionID")] Profesional profesional)
        {
            if (profesional.Calificacion < 1 || profesional.Calificacion > 5)
            {
                ModelState.AddModelError("ErrorCalificacion", "La calificación debe estar entre 1 y 5");
            }

            if (profesional.ValorHora < 1 || profesional.ValorHora > 9999)
            {
                ModelState.AddModelError("ErrorTarifa", "La tarifa por hora debe estar entre 1 y 9999");
            }

            if (ModelState.IsValid)
            {
                _context.Add(profesional);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ImagenID"] = new SelectList(_context.Imagenes, "ID", "Nombre", profesional.ImagenID);
            ViewData["ProfesionID"] = new SelectList(_context.Profesiones, "ID", "Descripcion", profesional.ProfesionID);
            return View(profesional);
        }

        // GET: Profesional/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales.FindAsync(id);
            if (profesional == null)
            {
                return NotFound();
            }
            ViewData["ImagenID"] = new SelectList(_context.Imagenes, "ID", "Nombre", profesional.ImagenID);
            ViewData["ProfesionID"] = new SelectList(_context.Profesiones, "ID", "Descripcion", profesional.ProfesionID);
            return View(profesional);
        }

        // POST: Profesional/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Nombre,Mail,Telefono,ValorHora,Calificacion,ImagenID,ProfesionID")] Profesional profesional)
        {
            if (id != profesional.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(profesional);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfesionalExists(profesional.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ImagenID"] = new SelectList(_context.Imagenes, "ID", "Nombre", profesional.ImagenID);
            ViewData["ProfesionID"] = new SelectList(_context.Profesiones, "ID", "Descripcion", profesional.ProfesionID);
            return View(profesional);
        }

        // GET: Profesional/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales
                .Include(p => p.Imagen)
                .Include(p => p.Profesion)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (profesional == null)
            {
                return NotFound();
            }

            return View(profesional);
        }

        // POST: Profesional/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var profesional = await _context.Profesionales.FindAsync(id);
            _context.Profesionales.Remove(profesional);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProfesionalExists(int id)
        {
            return _context.Profesionales.Any(e => e.ID == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProServicesWebsite.Models;
using ProServicesWebsite.Database;
using Microsoft.EntityFrameworkCore;

namespace ProServicesWebsite.Controllers
{
    public class HomeController : Controller
    {

        private readonly ProfesionalesDbContext _context;

        public HomeController(ProfesionalesDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var profesionalesDbContext = _context.Profesionales
                .Include(p => p.Imagen)
                .Include(p => p.Profesion);
            return View(await profesionalesDbContext
                .OrderByDescending(p => p.ID).ToListAsync());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
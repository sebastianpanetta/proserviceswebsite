﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProServicesWebsite.Models;
using ProServicesWebsite.Database;
using Microsoft.EntityFrameworkCore;

namespace ProServicesWebsite.Controllers
{
    public class ProfesionalesController : Controller
    {
        private readonly ProfesionalesDbContext _context;

        public ProfesionalesController(ProfesionalesDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var profesionalesDbContext = _context.Profesionales
                .Include(p => p.Imagen)
                .Include(p => p.Profesion);
            return View(await profesionalesDbContext
                .OrderBy(p => p.ID).ToListAsync());
        }
    }
}
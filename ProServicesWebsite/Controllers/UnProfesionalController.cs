﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProServicesWebsite.Database;
using ProServicesWebsite.Models;

namespace ProServicesWebsite.Controllers
{
    public class UnProfesionalController : Controller
    {
        private readonly ProfesionalesDbContext _context;

        public UnProfesionalController(ProfesionalesDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View("UnProfesional");
        }

        // GET: UnProfesional/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales
                .Include(p => p.Imagen)
                .Include(p => p.Profesion)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (profesional == null)
            {
                return NotFound();
            }

            return View(profesional);
        }

        // GET: UnProfesional/Contratar/5
        public IActionResult Contratar(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.ProfesionalID = id;
                return View();
            } else
            {
                return NotFound();
            }
        }

        // POST: UnProfesional/Contratar
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Contratar([Bind("ID,UsuarioID,ProfesionalID")] Contrato contrato)
        {
            // busqueda del ID de usuario logueado filtrando por mail
            var loggedUser = _context.Usuarios.FirstOrDefaultAsync(p => p.Email == User.Identity.Name); 
            contrato.UsuarioID = loggedUser.Result.Id;

            if (ModelState.IsValid)
            {
                _context.Add(contrato);
                await _context.SaveChangesAsync();
                return RedirectToAction("Contratados", "Usuarios");
            }
            ViewData["ProfesionalID"] = new SelectList(_context.Profesionales, "ID", "Mail", contrato.ProfesionalID);
            ViewData["UsuarioID"] = new SelectList(_context.Usuarios, "Id", "Email", contrato.UsuarioID);
            return View(contrato);
        }

    }
}
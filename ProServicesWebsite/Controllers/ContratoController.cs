﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProServicesWebsite.Database;
using ProServicesWebsite.Models;

namespace ProServicesWebsite.Controllers
{
    public class ContratoController : Controller
    {
        private readonly ProfesionalesDbContext _context;

        public ContratoController(ProfesionalesDbContext context)
        {
            _context = context;
        }

        // GET: Contrato
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                var profesionalesDbContext = _context.Contratos.Include(c => c.Profesional).Include(c => c.Usuario);
                return View(await profesionalesDbContext.ToListAsync());
            } 
            else
            {
                return NotFound();
            }
            
        }

        // GET: Contrato/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var contrato = await _context.Contratos
                .Include(c => c.Profesional)
                .Include(c => c.Usuario)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (contrato == null)
            {
                return NotFound();
            }

            return View(contrato);
        }

        // GET: Contrato/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var contrato = await _context.Contratos.FindAsync(id);
            if (contrato == null)
            {
                return NotFound();
            }
            ViewData["ProfesionalID"] = new SelectList(_context.Profesionales, "ID", "Mail", contrato.ProfesionalID);
            ViewData["UsuarioID"] = new SelectList(_context.Usuarios, "Id", "Email", contrato.UsuarioID);
            return View(contrato);
        }

        // POST: Contrato/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,UsuarioID,ProfesionalID")] Contrato contrato)
        {
            if (id != contrato.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contrato);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContratoExists(contrato.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfesionalID"] = new SelectList(_context.Profesionales, "ID", "Mail", contrato.ProfesionalID);
            ViewData["UsuarioID"] = new SelectList(_context.Usuarios, "Id", "Email", contrato.UsuarioID);
            return View(contrato);
        }

        // GET: Contrato/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || User.Identity.IsAuthenticated)
            {
                return NotFound();
            }

            var contrato = await _context.Contratos
                .Include(c => c.Profesional)
                .Include(c => c.Usuario)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (contrato == null)
            {
                return NotFound();
            }

            return View(contrato);
        }

        // POST: Contrato/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contrato = await _context.Contratos.FindAsync(id);
            _context.Contratos.Remove(contrato);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContratoExists(int id)
        {
            return _context.Contratos.Any(e => e.ID == id);
        }
    }
}

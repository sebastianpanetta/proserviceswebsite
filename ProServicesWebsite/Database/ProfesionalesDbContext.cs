﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProServicesWebsite.Models;
using Microsoft.EntityFrameworkCore;

namespace ProServicesWebsite.Database
{
    public class ProfesionalesDbContext : DbContext
    {
        public ProfesionalesDbContext(DbContextOptions<ProfesionalesDbContext> options) : base(options)
        {

        }

        public DbSet<Profesion> Profesiones { get; set; }
        public DbSet<Profesional> Profesionales { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Imagen> Imagenes { get; set; }
    }
}
